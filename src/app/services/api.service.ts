import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';

const PATH_CITIES = "/cities";
const PATH_SEARCH_CITIES = "/cities/search";

@Injectable({ providedIn: 'root' })
export class ApiService {
    constructor(
        private readonly http: HttpClient,
    ) { }

    private getHttpHeader(): HttpHeaders {
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        return headers;
    }

    private getParamsQuery(params: any) {
        if (!params) {
            return "";
        }

        let paramsArray = [];
        for (var key of Object.keys(params)) {
            paramsArray.push(`${encodeURI(key)}=${encodeURI(params[key])}`);
        }

        if (paramsArray.length == 0) {
            return "";
        }
        return '?' + paramsArray.join("&");
    }

    getCities() {
        return this.http.get(environment.API_URL + PATH_CITIES, { "headers": this.getHttpHeader(), observe: 'response' });
    }

    searchCities(searchTerm: string) {
        const url = environment.API_URL + PATH_SEARCH_CITIES + this.getParamsQuery({"term": searchTerm});
        return this.http.get(url, { "headers": this.getHttpHeader(), observe: 'response' });
    }

    postCities(data: FormData) {
        return this.http.post(environment.API_URL + PATH_CITIES, data, { observe: 'events', reportProgress: true });
    }
}
