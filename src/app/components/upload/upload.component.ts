import { HttpEvent, HttpEventType, HttpProgressEvent } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';

@Component({
    selector: 'app-upload',
    templateUrl: './upload.component.html',
    styles: [
    ]
})
export class UploadComponent implements OnInit {

    status: string = "";
    uploadProgress: number = 0;
    uploading: boolean = false;
    file : File | null = null;

    constructor(private readonly apiService: ApiService) { }

    ngOnInit(): void {
    }

    openFilePicker() {

    }

    handleFileInput($event: any) {
        this.file = $event?.target?.files?.item(0) ?? null;
    }

    uploadFile() {
        if (!this.file) {
            return;
        }

        this.status = "Uploading...";
        this.uploading = true;

        const formData = new FormData();
        formData.append('cities', this.file, this.file.name);

        this.apiService.postCities(formData).subscribe( {
            next: (event) => {
                if (event.type == HttpEventType.UploadProgress) {
                    if (event.total) {
                        this.uploadProgress = Math.round(100 * (event.loaded / event.total));
                        if (this.uploadProgress == 100)
                            this.status = `Processing...`;
                        else
                            this.status = `Uploaded ${this.uploadProgress} %`;
                        console.log(this.status);
                    }
                }
                else if (event.type == HttpEventType.Response) {
                    this.uploading = false;
                    this.status = "Upload succeeded";
                }
            },
            error: (error) => {
                this.uploading = false;
                console.log(error);
                this.status = `Upload failed: ${error.statusText}`;
            }
        });
    }
}
