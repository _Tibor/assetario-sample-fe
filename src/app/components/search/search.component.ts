import { Component, OnInit, ElementRef, AfterViewInit, ViewChild } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';

import { fromEvent } from 'rxjs';
import { debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';

import { NestedTreeControl } from '@angular/cdk/tree';
import { MatTreeNestedDataSource } from '@angular/material/tree';

interface TreeNode {
    name: string;
    children?: TreeNode[];
}

@Component({
    selector: 'app-search',
    templateUrl: './search.component.html',
    styles: [
        `.example-tree-invisible {
            display: none;
          }
          
          .example-tree ul,
          .example-tree li {
            margin-top: 0;
            margin-bottom: 0;
            list-style-type: none;
          }
          
          .example-tree .mat-nested-tree-node div[role=group] {
            padding-left: 40px;
          }
          
          .example-tree div[role=group] > .mat-tree-node {
            padding-left: 40px;
          }`
    ]
})
export class SearchComponent implements OnInit {

    searchTerm: string = "";
    searching: boolean = false;
    status: string = "";

    @ViewChild('searchInput') searchInput!: ElementRef;

    treeControl = new NestedTreeControl<TreeNode>(node => node.children);
    dataSource = new MatTreeNestedDataSource<TreeNode>();

    constructor(private readonly apiService: ApiService) { }

    ngOnInit(): void {
    }

    ngAfterViewInit() {
        // server-side search
        fromEvent(this.searchInput.nativeElement, 'keyup')
            .pipe(
                //filter(Boolean),
                debounceTime(250),
                distinctUntilChanged(),
                tap((text) => {
                    this.searchTermChanged();
                })
            )
            .subscribe();
    }

    searchTermChanged() {
        if (this.searchTerm.length < 3 || this.searching) {
            this.status = "";
            this.setTreeData([]);
            return;
        }
        this.searching = true;
        this.status = `Searching...`;
        console.log(`Search for: ${this.searchTerm}`);

        this.apiService.searchCities(this.searchTerm).subscribe({
            next: (result) => {
                this.searching = false;
                this.status = "Done";
                this.handleResponse(result.body as any);

            },
            error: (error) => {
                this.searching = false;
                console.log(error);
                this.status = `Search failed: ${error.statusText}`;
            }
        });
    }

    clearSearchTerm() {
        this.searchTerm = "";
        this.status = "";
        this.setTreeData([]);
    }

    private setTreeData(data: TreeNode[]) {
        this.dataSource.data = data;
        this.treeControl.dataNodes = data; // workaround to use Expand all. see: https://github.com/angular/components/issues/12469
    }

    hasResult() {
        return (this.dataSource?.data && this.dataSource.data.length > 0);
    }

    private handleResponse(response: any) {
        if (!response) {
            return;
        }

        this.status = `Found ${response.count} results.`;

        const result: TreeNode[] = [];

        response.cities?.forEach((data: any) => {
            if (Object.keys(data).length != 1) {
                return;
            }

            const name = Object.keys(data)[0];
            let children = data[name];
            if (!Array.isArray(children)) {
                return;
            }

            const res = this.addTreeNode(name, children);
            if (res)
                result.push(res);
        });

        this.setTreeData(result);
    }

    private addTreeNode(name: string, children: any[]): TreeNode | null {

        let res: TreeNode = {
            name: name,
            children: []
        };

        children?.forEach(d => {

            if (Object.keys(d).findIndex(k => k == "city") > -1) {
                res.children?.push({ name: d["city"], children: [] });
                return;
            }

            const childName = Object.keys(d)[0];
            if (Array.isArray(d[childName])) {
                const child = this.addTreeNode(childName, d[childName]);
                if (child)
                    res.children?.push(child);
            }
        });

        return res;
    }

    hasChild = (_: number, node: TreeNode) => !!node.children && node.children.length > 0;
}
